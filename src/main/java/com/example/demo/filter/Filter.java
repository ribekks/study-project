package com.example.demo.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Filter {

    private String name;

    private String surname;
}
