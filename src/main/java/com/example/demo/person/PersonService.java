package com.example.demo.person;

import com.example.demo.specification.Specifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> findPeopleMatch(String value){
       return personRepository.findAll(Specifications.likeFirstName(value));
    }

    public List findPeopleByParams(Specification specification){
        return personRepository.findAll(specification);
    }
}
