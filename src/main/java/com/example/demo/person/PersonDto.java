package com.example.demo.person;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonDto {

    private Integer id;

    private String name;

    private String surname;

}
