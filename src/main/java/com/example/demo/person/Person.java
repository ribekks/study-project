package com.example.demo.person;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@FieldNameConstants
@Entity
public class Person {

    @Id
    private Integer id;

    private String name;

    private String surname;
}
