package com.example.demo.person;

import com.example.demo.filter.Filter;
import com.example.demo.filter.FilterToSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping("/getByName/{name}")
    public void getPeopleBySearchString(@PathVariable String name){
        List<Person> person = personService.findPeopleMatch(name);
    }

    @PostMapping("/getByAll/{name}/{surname}")
    public void getPeopleByAllParams(@PathVariable String name, @PathVariable String surname){
        Filter filter = new Filter();
        filter.setName(name);
        filter.setSurname(surname);
        List<Person> person = personService.findPeopleByParams(FilterToSpec.getPerson(filter));
        System.out.println(person);
    }
}
