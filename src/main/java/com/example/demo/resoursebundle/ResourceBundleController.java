package com.example.demo.resoursebundle;

import com.example.demo.person.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("resource_bundle")
public class ResourceBundleController {

    @Autowired
    private ResourceBundleDemo resourceBundleDemo;

    @GetMapping("/get/{language}")
    public void createObject(@PathVariable String language){
        Person resourceBundled = resourceBundleDemo.getObject(language);
        System.out.println(resourceBundled.getName());
    }
}
