package com.example.demo.resoursebundle;

import com.example.demo.person.Person;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ResourceBundleDemo {

    private static final String BASE_NAME = "text";
    private Map<String, Object> resource;

    public Person getObject(String language){
        Person person = new Person();
        Locale locale = new Locale(language);
        ResourceBundle rb = ResourceBundle.getBundle(BASE_NAME, locale);
        for (String key : rb.keySet()) {
            if(key != null) {
                person.setName(rb.getString(key));
            }
        }
        return person;
    }
}
