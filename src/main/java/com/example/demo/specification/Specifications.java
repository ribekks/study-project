package com.example.demo.specification;

import com.example.demo.person.Person;
import org.springframework.data.jpa.domain.Specification;

public class Specifications {

    public static <T> Specification<T> likeFirstName (String value){
        if (value == null){
            return null;
        }
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get(Person.Fields.name), "%"+value+"%"));
    }

    public static <T> Specification<T> likeSurname (String value){
        if (value == null){
            return null;
        }
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get(Person.Fields.surname), "%"+value+"%"));
    }
}
