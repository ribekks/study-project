CREATE TABLE PERSON (
                        id   INTEGER NOT NULL AUTO_INCREMENT,
                        name VARCHAR,
                        surname VARCHAR,
                        PRIMARY KEY (id)
);
